RUNNING THE FORMAL VERIFICATION MODELS

1 - Install ProVerif 2.00. You can use the guidelines provided in the tool's
    official manual:
        https://prosecco.gforge.inria.fr/personal/bblanche/proverif/manual.pdf

    For Windows users, be sure also to perform the steps described in the manual
    for installing Graphviz and GTK+2.24. This allows ProVerif to generate
    graphical representations of the traces that it can possibly find.

2 - After installing ProVerif in a folder of your choice, select the protocols
    that you want to run the formal verification. Move their folders to a
    place of your choice inside the installation folder of ProVerif.

3 - In a terminal window (e.g. cmd on Windows), move to the ProVerif installation
    directory, for example:
            cd [YOURPATH]\proverif2.00

    In my case, I decided to separate the protocols in a sub folder on the root,
    which I called 'protocols-wsn'. Inside of this folder, each protocol has its 
    own folder, where there is a .pv file containing the model of its respective
    protocol, together with a .css file, which is used for formatting the content
    to be presented after running the formal verification. Running the formal
    verification would look something like this for each protocol:

    proverif -html ./protocols-wsn/tpbc ./protocols-wsn/tpbc/tinypbc.pv

    proverif -html ./protocols-wsn/najmussaqib ./protocols-wsn/najmussaqib/najmussaqib.pv

    proverif -html ./protocols-wsn/herrerahu ./protocols-wsn/herrerahu/herrerahu.pv

4 - After ProVerif finishes executing, a number of files will be generated
    in the protocol folder. In order to visualize the results, open
    the index.html in a browser of your preference.
